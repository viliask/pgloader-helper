<?php

namespace App\Controller;

use App\Entity\MigrationForm;
use App\Form\MigrationFormType;
use App\Repository\MigrationFormRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Datetime;

/**
 * @Route("/migration/form")
 */
class MigrationFormController extends AbstractController
{
    /**
     * @Route("/", name="migration_form_index", methods={"GET"})
     */
    public function index(MigrationFormRepository $migrationFormRepository): Response
    {
        return $this->render('migration_form/index.html.twig', [
            'migration_forms' => $migrationFormRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="migration_form_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $migrationForm = new MigrationForm();
        $form = $this->createForm(MigrationFormType::class, $migrationForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $migrationForm->setDate(new DateTime());
            exec("sudo -u ".$migrationForm->getLogin()
            ." psql -tc \"SELECT 1 FROM pg_database WHERE datname = '"
            .$migrationForm->getDbName()."'\" | grep -q 1 || sudo -u ".$migrationForm->getLogin()
            ." psql -c \"CREATE DATABASE "
            .$migrationForm->getDbName()."\"");

            $cmdPart = " "
            .$migrationForm->getDbType()."://"
            .$migrationForm->getLogin()
            .':'.$migrationForm->getPassword()
            .'@localhost/'.$migrationForm->getDbName()
            .' pgsql:///'.$migrationForm->getDbName()."'";

            $cmd = "sudo su -l ".$migrationForm->getUserName()
            ." -s /bin/bash -c ' pgloader".$cmdPart;

            while (@ ob_end_flush()); // end all output buffers if any

            $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');
            $live_output     = "";
            $complete_output = "";

            while (!feof($proc)){
                $live_output     = fread($proc, 4096);
                $complete_output = $complete_output . $live_output;
                @ flush();
            }

            pclose($proc);
            preg_match('/[0-9]+$/', $complete_output, $matches);

            if( intval($matches[0]) == 0 ){
                $migrationForm->setIsSuccessful('Yes');
            }
            else
                $migrationForm->setIsSuccessful('Not');
                
            $entityManager->persist($migrationForm);
            $entityManager->flush();
            return $this->render('migration_form/result.html.twig', [
                'complete_output' => $complete_output,
            ]);
        }

        return $this->render('migration_form/new.html.twig', [
            'migration_form' => $migrationForm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="migration_form_show", methods={"GET"})
     */
    public function show(MigrationForm $migrationForm): Response
    {
        return $this->render('migration_form/show.html.twig', [
            'migration_form' => $migrationForm,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="migration_form_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MigrationForm $migrationForm): Response
    {
        $form = $this->createForm(MigrationFormType::class, $migrationForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('migration_form_index', [
                'id' => $migrationForm->getId(),
            ]);
        }

        return $this->render('migration_form/edit.html.twig', [
            'migration_form' => $migrationForm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="migration_form_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MigrationForm $migrationForm): Response
    {
        if ($this->isCsrfTokenValid('delete'.$migrationForm->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($migrationForm);
            $entityManager->flush();
        }

        return $this->redirectToRoute('migration_form_index');
    }
}
