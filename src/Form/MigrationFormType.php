<?php

namespace App\Form;

use App\Entity\MigrationForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MigrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName', TextType::class , ['label' => 'User name (default: postgres)'])
            ->add('dbType', ChoiceType::class, [
                'required'   => true,
                'choices' => [
                    'MySQL' => 'mysql',
                    'SQLite' => 'sqlite',
                    'MS SQL Server'   => 'mssql',
                ]])
            ->add('login')
            ->add('password', PasswordType::class)
            ->add('dbName')
            ->add('description')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MigrationForm::class,
        ]);
    }
}
