<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\MigrationForm;
use \DateTime;

class MigrationFormFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 20; $i++) {
            $form = new MigrationForm();
            $form->setUserName('User '.$i);
            $form->setLogin('login'.$i);
            $form->setPassword('password'.$i);
            $form->setDbName('db'.$i);
            $form->setDate(new DateTime('now'));
            $manager->persist($form);
        }

        $manager->flush();
    }
}

