<?php

namespace App\Repository;

use App\Entity\MigrationForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MigrationForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method MigrationForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method MigrationForm[]    findAll()
 * @method MigrationForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MigrationFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MigrationForm::class);
    }

    // /**
    //  * @return MigrationForm[] Returns an array of MigrationForm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MigrationForm
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
